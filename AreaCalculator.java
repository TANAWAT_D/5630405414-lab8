package dathtanim.tanawat.lab6;

/*
 * AreaCalculator
 * 
 * 
 * @author Tanawat Dathtanim
 * @version 1.0
 * 
 */

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

public class AreaCalculator extends JFrame {

	private static final long serialVersionUID = 1L;

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});

	}

	protected JLabel lengthLabel;
	protected JLabel widthLabel;
	protected JLabel areaLabel;
	protected JTextField lengthText;
	protected JTextField widthText;
	protected JTextField area;
	protected JButton calculate;
	protected JButton reset;
	protected JButton exit;
	JPanel panel = new JPanel();
	JPanel panelLength = new JPanel(new GridLayout(1, 2));
	JPanel panelWidth = new JPanel(new GridLayout(1, 2));
	JPanel panelArea = new JPanel(new GridLayout(1, 2));
	JPanel panelButton = new JPanel(new FlowLayout());
	JPanel panelAll = new JPanel(new GridLayout(4, 1));

	protected AreaCalculator(String title) {
		super(title);
	}

	protected static void createAndShowGUI() {

		AreaCalculator acc = new AreaCalculator("Area Calculator");
		acc.addComponents();
		acc.setFrameFeatures();
		acc.addMenus();

	}

	protected void addMenus() {

	}

	protected void setFrameFeatures() {

		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		setVisible(true);
		pack();
		int w = getSize().width;
		int h = getSize().height;
		int x = (dim.width - w) / 2;
		int y = (dim.height - h) / 2;
		setLocation(x, y);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

	protected void addComponents() {

		lengthLabel = new JLabel("Length:");
		widthLabel = new JLabel("Width:");
		lengthText = new JTextField("0.0");
		lengthText.setHorizontalAlignment(SwingConstants.RIGHT);
		widthText = new JTextField("0.0");
		widthText.setHorizontalAlignment(SwingConstants.RIGHT);
		areaLabel = new JLabel("Area:");
		area = new JTextField("0.0");
		area.setHorizontalAlignment(SwingConstants.RIGHT);
		area.setBackground(Color.LIGHT_GRAY);
		area.setEditable(false);
		calculate = new JButton("Calculate");
		reset = new JButton("Reset");
		exit = new JButton("Exit");

		panelLength.add(lengthLabel);
		panelLength.add(lengthText);
		panelWidth.add(widthLabel);
		panelWidth.add(widthText);
		panelArea.add(areaLabel);
		panelArea.add(area);
		panelButton.add(calculate);
		panelButton.add(reset);
		panelButton.add(exit);

		panelAll.add(panelLength);
		panelAll.add(panelWidth);
		panelAll.add(panelArea);
		panelAll.add(panelButton);
		this.add(panelAll);

	}

}
