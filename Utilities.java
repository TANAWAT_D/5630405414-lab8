package dathtanim.tanawat.lab8;

public class Utilities {

	public static boolean isNumeric(String str) {

		try {
			Double.parseDouble(str);
		} catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}

}
