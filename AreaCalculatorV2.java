package dathtanim.tanawat.lab8;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import dathtanim.tanawat.lab6.AreaCalculator;

public class AreaCalculatorV2 extends AreaCalculator {

	private static final long serialVersionUID = 1L;

	double lengthValue;
	double widthValue;
	double areaValue;

	protected AreaCalculatorV2(String title) {
		super(title);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	protected static void createAndShowGUI() {
		AreaCalculatorV2 acc2 = new AreaCalculatorV2("Area Calculator V2");
		acc2.addComponents();
		acc2.setFrameFeatures();
	}

	protected void addComponents() {
		super.addComponents();
		calculate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				if (Utilities.isNumeric(lengthText.getText()) == true && Utilities.isNumeric(widthText.getText())) {
					lengthValue = Double.parseDouble(lengthText.getText());
					widthValue = Double.parseDouble(widthText.getText());
					if (lengthValue > 0 && widthValue > 0) {
						areaValue = lengthValue * widthValue;
						area.setText(Double.toString(areaValue));
					} else {
						JOptionPane.showMessageDialog(null, "Please enter only positive number");
					}
				} else {
					JOptionPane.showMessageDialog(null, "Please enter only positive number");
				}
			}
		});
		reset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lengthText.setText("");
				widthText.setText("");
				area.setText("");
			}
		});
		exit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				System.exit(0);
			}
		});
	}
}
