package dathtanim.tanawat.lab6;

/*
 * MySocialMedia
 * 
 * 
 * @author Tanawat Dathtanim
 * @version 1.0
 * 
 */

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

public class MySocialMedia extends MyFrame {

	private static final long serialVersionUID = 1L;
	public JTextArea area;
	protected JButton buttonPost;
	JPanel panel = new JPanel();
	protected JPanel panelcombobox = new JPanel(new GridLayout(1, 3));
	public JComboBox<String> feeling,privacy;
	
	protected MySocialMedia(String title) {
		super(title);

	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});

	}

	protected static void createAndShowGUI() {

		MySocialMedia msm = new MySocialMedia("My Social Media");
		msm.addComponents();
		msm.setFrameFeatures();

	}

	protected void addComponents() {

		area = new JTextArea("What's on your mind?", 5, 30);
		buttonPost = new JButton("Post");

		feeling = new JComboBox<String>();
		feeling.addItem("Excited");
		feeling.addItem("Happy");
		feeling.addItem("Sad");
		feeling.addItem("Angry");
		feeling.addItem("Thankful");
		feeling.addItem("Post");
		feeling.setEditable(true);

		privacy = new JComboBox<String>();
		privacy.addItem("Friends");
		privacy.addItem("Public");
		privacy.addItem("Only me");
		privacy.setEditable(true);

		panelcombobox.add(createSquareJPanel(Color.YELLOW, 10, feeling));
		panelcombobox.add(createSquareJPanel(Color.GREEN, 10, privacy));
		panelcombobox.add(createSquareJPanel(Color.BLUE, 10, buttonPost));
		this.add(area, BorderLayout.NORTH);
		this.add(panelcombobox, BorderLayout.CENTER);

	}

}
