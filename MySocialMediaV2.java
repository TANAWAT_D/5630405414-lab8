package dathtanim.tanawat.lab8;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;

import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;


import javax.imageio.ImageIO;
import javax.swing.JList;

import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import dathtanim.tanawat.lab6.MySocialMedia;

public class MySocialMediaV2 extends MySocialMedia {

	private static final long serialVersionUID = 1L;

	protected String labels[] = { "Facebook", "Twitter", "Google+" };
	protected JList<String> list;
	protected ListSelectionModel listSelectionModel;
	protected JPanel postsPanel;
	protected JPanel southPanel;
	protected NewPost newPost;
	protected StringBuffer select;
	protected String selectList;

	protected MySocialMediaV2(String title) {
		super(title);

	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	protected static void createAndShowGUI() {
		MySocialMediaV2 msm2 = new MySocialMediaV2("My Social Media V2");
		msm2.addComponents();
		msm2.setFrameFeatures();
		msm2.addListeners();
	}

	protected void addComponents() {
		super.addComponents();		
		southPanel = new JPanel(new BorderLayout());
		list = new JList<String>(labels);
		postsPanel = new JPanel(new GridLayout(5, 1));
		select = new StringBuffer();
		southPanel.add(list, BorderLayout.NORTH);
		southPanel.add(postsPanel, BorderLayout.SOUTH);
		listSelectionModel = list.getSelectionModel();
		listSelectionModel.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		list.setSelectedIndex(0);
		this.add(southPanel, BorderLayout.SOUTH);
	}

	public void crtNewPost(Color color) {
		String feel = (String) feeling.getSelectedItem().toString();
		String fileName = ("images/" + feel + ".png");
		String share = (String) privacy.getSelectedItem();
		int[] selections = list.getSelectedIndices();
		List<String> selectListt = list.getSelectedValuesList();
		int n = selections.length;
		select.setLength(0);
		for (int i = 0; i < n; i++) {
			select.append(selectListt.get(i));
		}
		selectList = select.toString();
		newPost = new NewPost(fileName, area.getText(), share, color);
		postsPanel.add(newPost);
		pack();
	}

	class ListSelectionHandler implements ListSelectionListener {
		public void valueChanged(ListSelectionEvent e) {
			if (!e.getValueIsAdjusting()) {
				crtNewPost(Color.blue);
			}
		}
	}

	class NewPost extends JPanel {
		
		private static final long serialVersionUID = 1L;
		BufferedImage img;
		int h, w;
		Rectangle2D.Double rect;
		String msg, privacy;
		Color color = Color.cyan;

		public NewPost(String fileName, String msg, String privacy, Color color) {
			this.msg = msg;
			this.color = color;
			this.privacy = privacy;
			try {
				
				img = ImageIO.read(new File(fileName));
				w = img.getWidth();
				h = img.getHeight();
				rect = new Rectangle2D.Double(0, 0, 400, h + 80);
			} catch (IOException e) {
				e.printStackTrace(System.err);
			}
		}

		public void paint(Graphics g) {
			Graphics2D g2d = (Graphics2D) g;
			g2d.setPaint(color);
			g2d.fill(rect);
			g2d.drawImage(img, 10, 10, null);
			g2d.setPaint(Color.BLACK);
			g2d.setFont(new Font("Serif", Font.BOLD, 16));
			g2d.drawString(msg + "", w + 10, h / 4);
			g2d.drawString("[For " + privacy + "]", w + 10, 2 * h / 4);
			g2d.drawString("Shared in " + selectList, w + 10, 3 * h / 4);
		}

		public Dimension getPreferredSize() {
			if (img == null)
				return new Dimension(100, 100);
			else
				return new Dimension(img.getWidth() + msg.length() + 300, img.getHeight() + 20);
		}
	}

	class MySocialMediaListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Object src = e.getSource();
			if (src == buttonPost || src == feeling || src == privacy) {
				crtNewPost(Color.yellow);
			}
		}
	}

	public void addListeners() {
		MySocialMediaListener msml = new MySocialMediaListener();
		listSelectionModel.addListSelectionListener(new ListSelectionHandler());
		buttonPost.addActionListener(msml);
		feeling.addActionListener(msml);
		privacy.addActionListener(msml);

	}

}