package dathtanim.tanawat.lab8;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import dathtanim.tanawat.lab6.Calculator;

public class CalculatorV2 extends Calculator {

	private static final long serialVersionUID = 1L;

	protected JMenuBar menu = new JMenuBar();
	protected JMenu file = new JMenu("File");
	protected JMenu config = new JMenu("Config");
	protected JMenuItem New = new JMenuItem("New");
	protected JMenuItem open = new JMenuItem("Open");
	protected JMenuItem save = new JMenuItem("Save");
	protected JMenuItem exit = new JMenuItem("Exit");
	protected JMenu setColor = new JMenu("Set Color");
	protected JMenuItem red = new JMenuItem("Red");
	protected JMenuItem green = new JMenuItem("Green");
	protected JMenuItem blue = new JMenuItem("Blue");
	double value1;
	double value2;
	double result;
	String operator = "";

	public CalculatorV2(String title) {
		super(title);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();

			}
		});
	}

	protected static void createAndShowGUI() {
		CalculatorV2 cal2 = new CalculatorV2("Calculator V2");
		cal2.addComponents();
		cal2.setFrameFeatures();
	}

	protected void addIcon(JMenuItem menuItem, String menuName, String iconFile) {
		menuItem.setName(menuName);
		ImageIcon pic = new ImageIcon(getClass().getResource(iconFile));
		menuItem.setIcon(pic);
	}

	protected void addMenu() {
		menu.add(file);
		menu.add(config);
		file.add(New);
		file.add(open);
		addIcon(open, "Open", "/images/open.png");
		file.add(save);
		file.add(exit);
		config.add(setColor);
		setColor.add(red);
		setColor.add(green);
		setColor.add(blue);
		red.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				inputText.setForeground(Color.RED);
			}
		});
		green.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				inputText.setForeground(Color.GREEN);
			}
		});
		blue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				inputText.setForeground(Color.BLUE);
			}
		});
		setJMenuBar(menu);
	}

	protected void addComponents() {
		super.addComponents();
		addMenu();
		button0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				inputText.setText("0");
				if (operator.equals("")) {
					value1 = 0;
				} else
					value2 = 0;
			}
		});
		button1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				inputText.setText("1");
				if (operator.equals("")) {
					value1 = 1;
				} else
					value2 = 1;
			}
		});
		button2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				inputText.setText("2");
				if (operator.equals("")) {
					value1 = 2;
				} else
					value2 = 2;
			}
		});
		button3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				inputText.setText("3");
				if (operator.equals("")) {
					value1 = 3;
				} else
					value2 = 3;
			}
		});
		button4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				inputText.setText("4");
				if (operator.equals("")) {
					value1 = 4;
				} else
					value2 = 4;
			}
		});
		button5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				inputText.setText("5");
				if (operator.equals("")) {
					value1 = 5;
				} else
					value2 = 5;
			}
		});
		button6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				inputText.setText("6");
				if (operator.equals("")) {
					value1 = 6;
				} else
					value2 = 6;
			}
		});
		button7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				inputText.setText("7");
				if (operator.equals("")) {
					value1 = 7;
				} else
					value2 = 7;
			}
		});
		button8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				inputText.setText("8");
				if (operator.equals("")) {
					value1 = 8;
				} else
					value2 = 8;
			}
		});
		button9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				inputText.setText("9");
				if (operator.equals("")) {
					value1 = 9;
				} else
					value2 = 9;
			}
		});
		buttonPlus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				operator = "+";
				inputText.setText(Double.toString(value1));
			}
		});
		buttonMinus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				operator = "-";
				inputText.setText(Double.toString(value1));
			}
		});
		buttonMultiply.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				operator = "*";
				inputText.setText(Double.toString(value1));
			}
		});
		buttonDivide.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				operator = "/";
				inputText.setText(Double.toString(value1));
			}
		});
		buttonMod.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				operator = "%";
				inputText.setText(Double.toString(value1));
			}
		});
		buttonEqual.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (operator.equals("+")) {
					result = value1 + value2;
				} else if (operator.equals("-")) {
					result = value1 - value2;
				} else if (operator.equals("*")) {
					result = value1 * value2;
				} else if (operator.equals("/")) {
					if (value2 == 0) {
						JOptionPane.showMessageDialog(null, "Divider cannot be zero");
					} else
						result = value1 / value2;
				} else if (operator.equals("%")) {
					result = value1 % value2;
				}
				inputText.setText(Double.toString(result));
			}
		});
	}
}
